#define ENCA 2 //Encoder cutter
#define ENCA2 3 // Encoder rotary
#define PWM 5 //PWM cutter
#define IN1 6 //Hbridge cutter
#define IN2 7 //Hbridge cutter
#define PWM2 11 //PWM rotary
#define IN4 10 //Hbridge rotary
#define IN3 9 //Hbridge rotary

int pulseCount,pulseCount2 = 0;
double Setpoint[4]={0};
int direction = 0;
long prevT,prevT2 = 0;
float eprev,eprev2 = 0;
float eintegral,eintegral2 = 0;

//*********************************/////////////////

//Values configuration
  //Minimun value for motors movement.
  int LowMotorLimit=40;
  //Cutter total amount of pulses to make a full cutting cycle.
  double ctarget = 21530; 
  //Rotary total amount of pulses to make 1 rotation.
  double rtarget=1000;

//**************************///////////////////

// functions
void setMotor(int dir, int pwmVal, int pwm, int in1, int in2){
  analogWrite(pwm,pwmVal);
  if(dir == 1){
    digitalWrite(in1,HIGH);
    digitalWrite(in2,LOW);
  }
  else if(dir == -1){
    digitalWrite(in1,LOW);
    digitalWrite(in2,HIGH);
  }
  else{
    digitalWrite(in1,LOW);
    digitalWrite(in2,LOW);
  }
}

void readEncoder(){
  if(direction==0){
    pulseCount++;}
  else pulseCount--;
}

void readEncoder2(){
    pulseCount2++;
}

void PID(int target, int motor){


  // PID constants
  float kp = 0.3; // To make a smooth output, decrease this value
  float kd = 0.0007; //
  float ki = 0.0; 

  // signal the motor
  if (motor == 1){
    //time difference
    long currT = micros();
    float deltaT = ((float) (currT - prevT))/( 1.0e6 );
    prevT = currT;

    int pos = pulseCount; 
  
    
    // error
    int e = pos - target;

    // derivative
    float dedt = (e-eprev)/(deltaT);

    // integral
    eintegral = eintegral + e*deltaT;

    // control signal
    float u = kp*e + kd*dedt + ki*eintegral;

    // motor power
    float pwr = fabs(u);
    if( pwr > 255 ){
      pwr = 255;
    }else if( pwr < LowMotorLimit ){
      pwr = LowMotorLimit;
    }

    
    // motor direction
    int dir = 1;
    if(u<0){
      dir = -1;
    }
    setMotor(dir,pwr,PWM,IN1,IN2);
    // store previous error
    eprev = e;

    Serial.print(target);
    Serial.print(" ");
    Serial.print(pos);
    Serial.print(" ");
    Serial.print(dir);
    Serial.print(" ");
    Serial.print(pwr);
    Serial.println();
    
  }else{      
    // time difference
    long currT2 = micros();
    float deltaT2 = ((float) (currT2 - prevT2))/( 1.0e6 );
    prevT2 = currT2;

    int pos2 = pulseCount2; 
  
    
    // error
    int e2 = pos2 - target;

    // derivative
    float dedt2 = (e2-eprev2)/(deltaT2);

    // integral
    eintegral2 = eintegral2 + e2*deltaT2;

    // control signal
    float u2 = kp*e2 + kd*dedt2 + ki*eintegral2;

    // motor power
    float pwr2 = fabs(u2);
    if( pwr2 > 255 ){
      pwr2 = 255;
    }else if( pwr2 < LowMotorLimit ){
      pwr2 = LowMotorLimit;
    }

    // motor direction
    int dir2 = 1;
    if(u2<0){
      dir2 = -1;
    }

    setMotor(dir2,pwr2,PWM2,IN3,IN4);
    // store previous error
    eprev2 = e2;

    Serial.print(target);
    Serial.print(" ");
    Serial.print(pos2);
    Serial.print(" ");
    Serial.print(dir2);
    Serial.print(" ");
    Serial.print(pwr2);
    Serial.println();
  }   

}

void Move_cutter(){
  direction=0;
  while (pulseCount < ctarget) {
    PID(ctarget,1);
  }
  delay(1000);
  direction=1;
  while (pulseCount > 0) {
    PID(0,1);
  }
  setMotor(0,0,PWM,IN1,IN2);
}

void Move_rotary(int target2){
  while (pulseCount2 < target2) {
    PID(target2,0);
  }
  setMotor(0,0,PWM2,IN3,IN4);
}

void Setpoints(){
//                Pulses per degree     Angle of each stage
  Setpoint[0]=      (rtarget/360)   *        153;
  Setpoint[1]=      (rtarget/360)   *        194;
  Setpoint[2]=      (rtarget/360)   *        153;
  Setpoint[3]=      (rtarget/360)   *        300;
}

//Main program

void setup()
{
  Serial.begin(9600);
  pinMode(ENCA,INPUT);
  pinMode(ENCA, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(ENCA),readEncoder,RISING);

  pinMode(PWM,OUTPUT);
  pinMode(IN1,OUTPUT);
  pinMode(IN2,OUTPUT);

  pinMode(ENCA2,INPUT);
  pinMode(ENCA2, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(ENCA2),readEncoder2,RISING);
  
  pinMode(PWM2,OUTPUT);
  pinMode(IN3,OUTPUT);
  pinMode(IN4,OUTPUT);

  Setpoints();
  
  Serial.println("Setup done.");

}

void loop()
{  
  Serial.println("Rotation in 5 seconds...");
  pulseCount2=0;
  delay(5000);
  Move_rotary(Setpoint[0]);//QR station
  delay(5000);
  Move_rotary(Setpoint[1]); //Cutter station
  delay(5000);
  Move_cutter(); //Cutter activation
  delay(5000);
  Move_rotary(Setpoint[2]); //Feeding station
  delay(5000);
  Move_rotary(Setpoint[3]); //Home station
  delay(5000);
  
}
